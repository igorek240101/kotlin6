// 1. You can declare isEven and isOdd as values that can be called as extension functions. Complete the declarations in the code.
fun task(): List<Boolean> {
    val isEven: Int.() -> Boolean = { this % 2 == 0 }
    val isOdd: Int.() -> Boolean = { this % 2 == 1 }

    return listOf(42.isOdd(), 239.isOdd(), 294823098.isEven())
}
// 2. Implement the function buildMutableMap that takes a parameter (of extension function type), creates a new HashMap, builds it, and returns it as a result. Note that starting from 1.3.70, the standard library has a similiar buildMap function.
import java.util.HashMap

fun buildMutableMap(put: HashMap<Int, String>.() -> Unit) : Map<Int, String>
{
    val hash = HashMap<Int, String>();
    hash.put();
    return hash;
}

fun usage(): Map<Int, String> {
    return buildMutableMap {
        put(0, "0")
        for (i in 1..10) {
            put(i, "$i")
        }
    }
}
// 3. The previous examples can be rewritten using the library function apply. Write your implementation of this function named myApply.
fun <T> T.myApply(f: T.() -> Unit): T {
    this.f();
    return this;
}

fun createString(): String {
    return StringBuilder().myApply {
        append("Numbers: ")
        for (i in 1..10) {
            append(i)
        }
    }.toString()
}

fun createMap(): Map<Int, String> {
    return hashMapOf<Int, String>().myApply {
        put(0, "0")
        for (i in 1..10) {
            put(i, "$i")
        }
    }
}
// 4. 1) Fill the table with proper values from the product list. The products are declared in data.kt.
// 2) Color the table like a chessboard. Use the getTitleColor() and getCellColor() functions. Pass a color as an argument to the functions tr, td.
// Run the main function defined in the file demo.kt to see the rendered table.
fun renderProductTable(): String {
    return html {
        table {
            tr(getTitleColor()) {
                td {
                    text("Product")
                }
                td {
                    text("Price")
                }
                td {
                    text("Popularity")
                }
            }
            val products = getProducts()
            var index : Int = 0;
            for (item in products)
            {
                tr{
                	td(getCellColor(index, 1)) {
                    		text(item.description)
                		}
                	td(getCellColor(index, 2)) {
                    		text(item.price)
                		}
                	td(getCellColor(index, 3)) {
                    		text(item.popularity)
                		}
            	}
                index++;
            }
        }
    }.toString()
}

fun getTitleColor() = "#b9c9fe"
fun getCellColor(index: Int, column: Int) = if ((index + column) % 2 == 0) "#dce4ff" else "#eff2ff"
// 5. Test
import Answer.*

enum class Answer { a, b, c }

val answers = mapOf<Int, Answer?>(
        1 to Answer.c, 2 to Answer.b, 3 to Answer.b, 4 to Answer.c
)
// 6. Complete the implementation of a simplified DSL for HTML. Implement tr and td functions.
open class Tag(val name: String) {
    protected val children = mutableListOf<Tag>()

    override fun toString() =
            "<$name>${children.joinToString("")}</$name>"
}

fun table(init: TABLE.() -> Unit): TABLE {
    val table = TABLE()
    table.init()
    return table
}

class TABLE : Tag("table") {
    fun tr(init: TR.() -> Unit) {
        val tr = TR()
        tr.init()
        children += tr
    }
}

class TR : Tag("tr") {
    fun td(init: TD.() -> Unit) {
        children += TD().apply(init)
    }
}

class TD : Tag("td")

fun createTable() =
        table {
            tr {
                repeat(2) {
                    td {
                    }
                }
            }
        }

fun main() {
    println(createTable())
    //<table><tr><td></td><td></td></tr></table>
}